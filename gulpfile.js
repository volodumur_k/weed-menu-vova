const gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    nunjucks = require('gulp-nunjucks-render'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    AWS = require('aws-sdk'),
    through = require('through2'),
    log = require('fancy-log'),
    zip = require('gulp-zip');


const errorHandler = (err) => {
    log.error(err);
};

const getChangedAssets = () => {
    const spawn = require('child_process').spawn;
    let files = [];
    return new Promise(resolve => {
        const cmd = spawn('git', ['show', '--name-status', '--oneline']);
        cmd.stdout.on('data', data => {
            files = data.toString().split('\n').slice(1, -1)
                .map(item => {
                    const result = item.split('\t');
                    return { status: result[0], path: result[1] };
                })
                .filter(fileInfo => fileInfo.path.startsWith('static/source/img/'));
        });
        cmd.on('close', code => {
            log(`Command exited with code: ${code}`);
            resolve(files);
        });
    });
};

const getAssetS3KeyByPath = path => {
    if (path.startsWith('static/source/img/')) {
        return 'assets/' + path.slice(14);
    }

    return path;
};

const deleteAssetsFromS3 = (files = [], bucket = 'weedmenu-dev') => {
    if (!Array.isArray(files) || files.length === 0) {
        return Promise.resolve();
    }

    return new Promise((resolve, reject) => {
        const S3 = new S3();
        gulp.src(files)
            .pipe(through.obj(async (file, encoding, callback) => {
                log.info('Deleting following file from S3: ', file.relative);
                try {
                    await S3.deleteObject({
                        Bucket: bucket,
                        Key: getAssetS3KeyByPath(file.relative),
                    }).promise();
                    callback();
                } catch (err) {
                    log.error('Error occurred during deleting object from S3: ', file.relative);
                    reject(err);
                }
            }))
            .on('error', reject)
            .on('end', resolve);
    });
};

const uploadAssetsToS3 = (files = [], bucket = 'weedmenu-dev') => {
    if (!Array.isArray(files) || files.length === 0) {
        return Promise.resolve();
    }
    return new Promise((resolve, reject) => {
        const S3 = new AWS.S3();
        return gulp.src(files, { base: '.' })
            .pipe(through.obj(async (file, encoding, callback) => {
                log.info('Preparing to upload ', file.relative);
                try {
                    await S3.upload({
                        Key: getAssetS3KeyByPath(file.relative),
                        Bucket: bucket,
                        Body: file.contents
                    }).promise();
                    callback();
                } catch (err) {
                    log.error('Error occurred during uploading to S3: ', file.relative);
                    reject(err);
                }

            }))
            .on('data', () => {})
            .on('end', () => {
                log('Finished stream...');
                resolve();
            })
            .on('error', reject);
    });
};

const invalidateCloudFrontAssets = files => {
    const cf = new AWS.CloudFront();
    const invalidationPaths = files.map(getAssetS3KeyByPath);
    log.info(invalidationPaths);
    return Promise.resolve();
};

const DEV_ASSETS_BUCKET = 'weedmenu-dev';
const DEV_CF_ASSETS_DISTRIBUTION = 'E1FRYI8UHJENTK';

AWS.config.apiVersions = {
    codedeploy: '2014-10-06',
    s3: '2006-03-01',
    cloudfront: '2017-10-30',
};

gulp.task('styles', () => {
    gulp.src(['styles.scss'], { cwd: 'static/source/scss/' })
        .pipe(plumber({
            errorHandler: errorHandler
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('static/dist/css'))
        .pipe(rename('wm-styles.min.css'))
        .pipe(gulp.dest('restomulti/assets/css'));
});

gulp.task('js', () => {
    const jsFiles = [
        'node_modules/owl.carousel/dist/owl.carousel.js',
        'node_modules/selectize/dist/js/standalone/selectize.js',
        'source/js/wm-custom.js',
    ];
    gulp.src(jsFiles)
        .pipe(plumber({errorHandler: errorHandler}))
        .pipe(sourcemaps.init())
        //.pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('static/dist/js'));
});

gulp.task('html', () => {
    return gulp.src('static/source/templates/*.html')
        .pipe(plumber({
            errorHandler: errorHandler
        }))
        .pipe(nunjucks({
            path: ['static/source/templates/']
        }))
        .pipe(gulp.dest('static/dist/'));
});

gulp.task('images', function () {
    return gulp.src('static/source/img/**/*.*')
        .pipe(gulp.dest('static/dist/img/'));
});

gulp.task('clear:dist', () => {
    return gulp.src('dist', { read: false })
        .pipe(plumber({errorHandler: errorHandler}))
        .pipe(clean());
});

gulp.task('copy:prod', ['clear:dist'], () => {
    gulp.src(['restomulti/**/*.*', 'restomulti/.htaccess', 'appspec.yml'])
        .pipe(gulp.dest('dist/'));

    return gulp.src('restomulti/protected/config/db.local.prod.php')
        .pipe(rename('db.local.php'))
        .pipe(gulp.dest('dist/protected/config/', { overwrite: true }));
});

gulp.task('copy', ['clear:dist'], () => {
     return gulp.src(['restomulti/**/*.*', 'restomulti/.htaccess', 'appspec.yml'])
        .pipe(gulp.dest('dist/'));
});

gulp.task('copy:dev', ['copy'], () => {
    return gulp.src('dist/protected/config/db.local.dev.php')
        .pipe(rename('db.local.php'))
        .pipe(gulp.dest('dist/protected/config/', { overwrite: true }));
});

gulp.task('deploy-app:dev', ['clear:dist', 'copy:dev'], async () => {
    return new Promise((resolve) => {
        let s3Location, codeDeploy;
        gulp.src('dist/**/*', { dot: true })
            .pipe(zip('WeedMenuApp.zip'))
            .pipe(through.obj(async (file, enc, cb) => {
                try {
                    const s3 = new AWS.S3();
                    const s3UploadResult = await s3.upload({ ACL: 'bucket-owner-read',
                        Key: `weedmenu.dev-${new Date().getTime()}.zip`,
                        Body: file.contents,
                        Bucket: 'weedmenu-codedeploy-dev'
                    }).promise();
                    log('Application uploaded to S3 successfully: ', s3UploadResult);
                    s3Location = {
                        bucket: s3UploadResult.Bucket,
                        bundleType: 'zip',
                        eTag: s3UploadResult.ETag,
                        key: s3UploadResult.Key
                    };
                    cb(null, s3Location);
                } catch (err) {
                    log.error('Error occurred during uploading to S3: ', err);
                    cb(err);
                }
            }))
            .pipe(through.obj(async (file, enc, cb) => {
                log(file);
                try {
                    codeDeploy = new AWS.CodeDeploy({ region: 'us-west-2' });
                    const appRevision = await codeDeploy.registerApplicationRevision({
                        applicationName: 'weedmenu-dev',
                        revision: {
                            revisionType: 'S3',
                            s3Location,
                        },
                        description: 'Deployed with gulp'
                    }).promise();

                    log('Application revision added: ', appRevision);
                    cb(null, appRevision);
                } catch (err) {
                    log.error('Error occurred during registering new application revision: ', err);
                    cb(err);
                }

            }))
            .pipe(through.obj(async (file, enc, cb) => {
                try {
                    const deploymentRes = await codeDeploy.createDeployment({
                        applicationName: 'weedmenu-dev',
                        deploymentGroupName: 'weedmenu-dev-dg',
                        revision: {
                            revisionType: 'S3',
                            s3Location,
                        }
                    }).promise();

                    log('New deployment has been created: ', deploymentRes);

                    while (true) {
                        const deploymentStatus = await codeDeploy.getDeployment({ deploymentId: deploymentRes.deploymentId }).promise();
                        const status = deploymentStatus.deploymentInfo.status;
                        log('Current deployment status is: ', status);
                        if (status === 'Succeeded' || status === 'Failed' || status === 'Stopped') {
                            break;
                        }

                        log('Sleeping 10 seconds...');
                        await new Promise(resolve => {
                            setTimeout(() => resolve(), 10000);
                        });
                    }
                    log('Deployment of new version is finished.');
                    cb(null);

                } catch (err) {
                    log.error('Error during creating new deployment: ', err);
                    cb(err)
                }
            }))
            .pipe(through.obj((file, enc, cb) => {
                log('Everything has been deployed');
                resolve();
            }));
    });
});

gulp.task('deploy-assets:dev', ['build'], async callback => {
    let staticAssets;
    try {
        staticAssets = await getChangedAssets();
        log(staticAssets);
    } catch (err) {
        log.error('Error occurred during getting changes: ', err);
        return callback(err);
    }

    const assetsToUpload = staticAssets.filter(fileInfo => fileInfo.status === 'A' || fileInfo.status === 'M')
        .map(fileInfo => fileInfo.path);
    const assetsToDelete = staticAssets.filter(fileInfo => fileInfo.status === 'D')
        .map(fileInfo => fileInfo.path);

    try {
        const res = await Promise.all([
            uploadAssetsToS3(assetsToUpload),
            deleteAssetsFromS3(assetsToDelete),
        ]);
        log('Finished uploading...', res);
    } catch (err) {
        log.error('Error occurred during updating S3: ', err);
        callback(err);
    }

    try {
        log.info('Invalidating CF...');
        await invalidateCloudFrontAssets(assetsToUpload);
    } catch (err) {
        log.error('Error while invalidating CF: ', err);
        callback(err);
    }

});

gulp.task('deploy-app:prod', ['clear:dist', 'copy:prod'], () => {

});

gulp.task('watch', function () {
    gulp.watch('static/source/scss/**/*.scss', ['styles']);
    gulp.watch('static/source/templates/**/*', ['html']);
    gulp.watch('static/source/img/**/*', ['images']);
    gulp.watch('static/source/js/**/*.js', ['js']);
});

gulp.task('default', ['styles', 'js', 'html', 'images', 'watch']);
gulp.task('build', ['styles', 'js', 'html', 'images']);
gulp.task('deploy:production', ['deploy-app:prod']);
gulp.task('deploy:dev', ['deploy-app:dev']);
